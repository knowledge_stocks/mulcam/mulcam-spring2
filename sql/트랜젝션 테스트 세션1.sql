﻿use mydb;

CREATE TABLE emp (
  name VARBINARY(20) DEFAULT NULL,
  price INT DEFAULT NULL
)

# autocommit 확인
SELECT @@autocommit;

# 현재 세션의 autocommit false로 변경
SET autocommit = 0;
SELECT @@autocommit;

# mysql은 isolation 속성이 REPEATABLE_READ으로 기본값이 설정되어 있다.
# 반면 oracle은 READ_COMMIT이 기본값으로 설정되어 있다.
##
# REPEATABLE_READ: 현재 트랜젝션이 데이터를 수정하지 않았다면 해당 데이터를 다시 읽어와도 동일한 데이터 출력되어야 한다.
# READ_UNCOMMITED: 다른 트랜젝션이 커밋하지 않은 데이터를 읽어온다.
# READ_COMMIT: 커밋한 데이터만 읽어올 수 있다.
# SERIALIZABLE: 트랜젝션이 시작되고 한번 접근한 데이터는 다른 세션에서 접근하지 못한다.(트랜젝션이 끝날때까지 대기함)

# ISOLATION 값을 바꾸고 트랜젝션을 시작시키며 테스트해보자.
##
# SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
# SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED;
# SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ;
# SET SESSION TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SELECT @@session.transaction_isolation;

# 트랜젝션 시작
# 이 문구를 넣으면 autocommit을 굳이 0으로 안 만들어도 되는거 아닌가?
START TRANSACTION;

# 데이터에 한번 접근하고 나면 해당 데이터에 ISOLATION 제약조건이 적용된다.
SELECT * FROM emp;

INSERT INTO emp(name, price) VALUES('park', 9000);

COMMIT;
ROLLBACK;