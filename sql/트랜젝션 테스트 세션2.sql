﻿use mydb;

SELECT @@session.transaction_isolation;

# 세션 1에서 명령을 수행해보면서 확인해보기
SELECT * FROM emp;

INSERT INTO emp(name, price) VALUES('lee', 10000);

# READ UNCOMMITTED으로 설정하면 다른 트랜젝션이 커밋하지 않은 데이터를 가져올 수 있다.
SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SELECT * FROM emp;

# 다시 isolation을 수정해보면 uncommited 데이터가 보이지 않는것을 확인할 수 있다.
SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ;
SELECT * FROM emp;