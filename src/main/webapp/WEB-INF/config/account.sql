﻿CREATE TABLE cust_account(
accountNo VARCHAR(20) PRIMARY KEY,
custName VARCHAR(50),
balance FLOAT(20));

INSERT INTO cust_account(accountNo, custName, balance)
VALUES('70-490-930','홍길동',10000000);

INSERT INTO cust_account(accountNo, custName, balance)
VALUES('70-490-911','김유신',10000000);

commit;
SELECT * FROM cust_account;
