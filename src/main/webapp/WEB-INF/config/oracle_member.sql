CREATE TABLE t_member(
	id VARCHAR2(10), 
	pwd VARCHAR2(10), 
	name VARCHAR2(50 ), 
	email VARCHAR2(50 BYTE), 
	joinDate DATE DEFAULT sysdate, 
	PRIMARY KEY (id));

INSERT INTO t_member
VALUES('hong','1212','홍길동','hong@gmail.com',sysdate);

INSERT INTO t_member
VALUES('lee','1212','이순신','lee@test.com', sysdate);

INSERT INTO t_member
VALUES('kim','1212','김유신','kim@jweb.com',sysdate);

SELECT * FROM t_member;
