<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	isELIgnored="false" 
%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"  />  
  
<html>
<head>

<c:choose>
   <c:when test='${msg=="added" }'>
      <script>
         window.onload=function(){
            alert("회원을 등록했습니다.");
         }
      </script>
   </c:when>
   <c:when test='${msg=="modified" }'>
      <script>
        window.onload=function(){
          alert("회원 정보를 수정했습니다.");
        }
      </script>
   </c:when>
   <c:when test= '${msg=="deleted" }'>
      <script>
         window.onload=function(){
            alert("회원 정보를 삭제했습니다.");
        } 
      </script>
	</c:when>
	<c:when test= '${msg=="failed" }'>
      <script>
         window.onload=function(){
            alert("요청이 실패했습니다.");
        } 
      </script>
	</c:when>
</c:choose>

   <meta  charset="UTF-8">
   <title>회원 정보 출력창</title>
<style>
     .cls1 {
       font-size:40px;
       text-align:center;
     }
    
     .cls2 {
       font-size:20px;
       text-align:center;
     }
  </style>
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script type="text/javascript">
	  $(document).ready(function() {
		  $('#delBtn').click(function() {
			  var chkList=[];
			  $('.delChk:checked').each(function() {
				  chkList.push($(this).val());
			  });
			  if(chkList.length==0) {
				  alert('삭제할 사용자를 선택해주세요.');
				  return;
			  }
			  $.ajax({
				  url: "removeSelected.my",
				  data: {
					  'ids': chkList,
				  },
				  success: function(data) {
					  location.reload();
				  },
				  error: function(data, status) {
					  console.log(data);
					  alert(data);
				  },
			  })
		  });
	  });
  </script>
  
</head>
<body>
  <div>
    <form action="list.my">
      <p><span>이름</span> <input type="text" name="name" /> <span>이메일</span> <input type="text" name="email" /> <input type="submit" value="검색" /></p>
    </form>
    <form action="list.my">
      <p>
        <select name="field">
          <option value="name">이름</option>
          <option value="email">이메일</option>
        </select>
        <input type="text" name="query" />
        <input type="submit" value="검색" />
       </p>
    </form>
  </div>
	<p class="cls1">
		회원정보<span class="cls2">(${members.size()}명)</span>
	</p>
  <p><a href="addMembers.my"><button>여러개 추가 테스트</button></a></p>
	<p><input id="delBtn" type="button" value="삭제"/></p>
	<table align="center" border="1">
		<tr align="center" bgcolor="lightgreen">
			<td width="7%"><b>선택</b></td>
			<td width="7%"><b>아이디</b></td>
			<td width="7%"><b>비밀번호</b></td>
			<td width="7%"><b>이름</b></td>
			<td width="7%"><b>이메일</b></td>
			<td width="7%"><b>가입일</b></td>
			<td width="7%"><b>수정</b></td>
			<td width="7%"><b>삭제</b></td>
		</tr>

		<c:choose>
			<c:when test="${empty  members}">
				<tr>
					<td colspan=5><b>등록된 회원이 없습니다.</b></td>
				</tr>
			</c:when>
			<c:when test="${!empty members}">
				<c:forEach var="mem" items="${members}">
					<tr align="center">
						<td><input class="delChk" type="checkbox" value="${mem.id}" /></td>
						<td>${mem.id }</td>
						<td>${mem.pwd }</td>
						<td>${mem.name}</td>
						<td>${mem.email }</td>
						<td>${mem.joinDate}</td>
						<td><a href="modify.my?id=${mem.id }">수정</a></td>
						<td><a href="remove.my?id=${mem.id }">삭제</a></td>
					</tr>
				</c:forEach>
			</c:when>
		</c:choose>
	</table>
	<a href="add.my"><p class="cls2">회원 가입하기</p></a>
</body>
</html>
