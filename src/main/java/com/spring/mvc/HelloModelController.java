package com.spring.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloModelController {

	// url 요청명과 같은 jsp 응답
	// Model만 사용하는 경우는 url-pattern을 /mvc/*로 설정했을 때, /로 설정했을 때 매핑하기가 곤란하다.
//	@RequestMapping("/helloModel")
	public Model search(Model model) {
		model.addAttribute("id", "Korea");

		return model;
	}
}
