package com.spring.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.spring.jdbc.entity.MemberVO;

@Controller
public class HelloCommandController {

	@RequestMapping(value = "/mem", method = RequestMethod.GET)
	public String form() {
		return "view/memForm";
	}

//	@RequestMapping(value = "/mem", method = RequestMethod.POST)
//	public ModelAndView submit(ModelAndView mv,
//			String id, String pwd, String name, String email) {
//		mv.addObject("id", id);
//		mv.addObject("pwd", pwd);
//		mv.addObject("name", name);
//		mv.addObject("email", email);
//		mv.setViewName("view/memResult");
//		
//		return mv;
//	}

//	@RequestMapping(value = "/mem", method = RequestMethod.POST)
//	public ModelAndView submit(ModelAndView mv, MemberVO vo) {
//		mv.addObject("vo", vo);
//		mv.setViewName("view/memResult");
//		
//		return mv;
//	}

	@RequestMapping(value = "/mem", method = RequestMethod.POST)
	public String submit(@ModelAttribute("vo")MemberVO vo) {
		return "view/memResult";
	}
}
