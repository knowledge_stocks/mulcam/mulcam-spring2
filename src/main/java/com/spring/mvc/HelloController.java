package com.spring.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloController {

	@RequestMapping("/hello")
	public String search() {
		int sum = 0;
		for (int i = 0; i < 5; i++) {
			sum += i;
			System.out.printf("i=%d sum=%d\n", i, sum);
		}

		return "view/hello"; // prefix는 "/WEB-INF/" suffix는 ".jsp"
	}
}
