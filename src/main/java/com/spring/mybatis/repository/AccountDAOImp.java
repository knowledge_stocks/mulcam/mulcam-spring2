package com.spring.mybatis.repository;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

@Repository
public class AccountDAOImp implements AccountDAO {
  @Autowired
  private SqlSession sqlSession;

  public AccountDAOImp() {
  }

  public void setSqlSession(SqlSession sqlSession) {
    this.sqlSession = sqlSession;
  }

  @Override
  public int updateBalance1() throws DataAccessException {
    return sqlSession.update("mapper.account.updateBalance1");
  }

  @Override
  public int updateBalance2() throws DataAccessException {
    return sqlSession.update("mapper.account.updateBalance2");
  }
}
