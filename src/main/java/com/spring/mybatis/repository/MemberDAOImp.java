package com.spring.mybatis.repository;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.mybatis.entity.MemberVO;

@Repository
public class MemberDAOImp implements MemberDAO {
	@Autowired
	private SqlSession sqlSession;

	public MemberDAOImp() {
	}

	public void setSqlSession(SqlSession sqlSession) {
		this.sqlSession = sqlSession;
	}
	
	@Override
	public List<MemberVO> list() {
		return list(null);
	}

	@Override
	public List<MemberVO> list(String name, String email) {
		MemberVO condition = new MemberVO();
		condition.setName(name);
		condition.setEmail(email);
		return list(condition);
	}
	
	@Override
	public List<MemberVO> list(MemberVO condition) {
		return sqlSession.selectList("mapper.member.listSelected", condition);
	}

	@Override
	public int insert(MemberVO vo) {
		return sqlSession.insert("mapper.member.add", vo);
	}
	
	@Override
	public int insertAll(List<MemberVO> members) {
	  return sqlSession.insert("mapper.member.addList", members);
	}

	@Override
	public int update(MemberVO vo) {
		return sqlSession.update("mapper.member.modify", vo);
	}

	@Override
	public int delete(String id) {
		return sqlSession.delete("mapper.member.remove", id);
	}
	
	@Override
	public int deleteAll(String[] ids) {
		return sqlSession.delete("mapper.member.removeSelected", ids);
	}

	@Override
	public MemberVO get(String id) {
		return sqlSession.selectOne("mapper.member.get", id);
	}

	@Override
	public int count() {
		return sqlSession.selectOne("mapper.member.count");
	}
}
