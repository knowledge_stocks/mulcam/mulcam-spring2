package com.spring.mybatis.repository;

import java.util.List;

import com.spring.mybatis.entity.MemberVO;

public interface MemberDAO {
	public List<MemberVO> list();
	
	public List<MemberVO> list(String name, String email);
	
	public List<MemberVO> list(MemberVO condition);

	public int insert(MemberVO vo);
	
	public int insertAll(List<MemberVO> members);

	public int update(MemberVO vo);

	public int delete(String id);
	
	public int deleteAll(String[] ids);

	public MemberVO get(String id);

	public int count();
}
