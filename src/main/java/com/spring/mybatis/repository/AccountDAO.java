package com.spring.mybatis.repository;

import org.springframework.dao.DataAccessException;

public interface AccountDAO {
  public int updateBalance1() throws DataAccessException;

  public int updateBalance2() throws DataAccessException;
}
