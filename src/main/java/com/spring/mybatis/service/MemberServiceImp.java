package com.spring.mybatis.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.mybatis.entity.MemberVO;
import com.spring.mybatis.repository.MemberDAO;

@Service
public class MemberServiceImp implements MemberService {
	@Autowired
	private MemberDAO memberDAO;

	public MemberServiceImp() {
	}

	public void setMemberDAO(MemberDAO memberDAO) {
		this.memberDAO = memberDAO;
	}

	@Override
	public List<MemberVO> listMembers(String name, String email) {
		return memberDAO.list(name, email);
	}

	@Override
	public int addMember(MemberVO memberVO) {
		return memberDAO.insert(memberVO);
	}
	
	@Override
	public int addMembers(List<MemberVO> members) {
	  return memberDAO.insertAll(members);
	}

	@Override
	public MemberVO getMember(String id) {
		return memberDAO.get(id);
	}

	@Override
	public int modMember(MemberVO vo) {
		return memberDAO.update(vo);
	}

	@Override
	public int removeMember(String id) {
		return memberDAO.delete(id);
	}
	
	@Override
	public int removeMembers(String[] ids) {
		return memberDAO.deleteAll(ids);
	}
	
	@Override
	public int countMember() {
		return memberDAO.count();
	}
}
