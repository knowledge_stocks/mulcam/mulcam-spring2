package com.spring.mybatis.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.spring.mybatis.repository.AccountDAO;

// 전체 메소드에 트랜젝션을 설정하고 싶은 경우
// @Transactional(propagation = Propagation.REQUIRED)
@Service
public class AccountServiceImp implements AccountService {
  @Autowired
  private AccountDAO accountDAO;

  public void setAccDAO(AccountDAO accountDAO) {
    this.accountDAO = accountDAO;
  }

  // 특정 메소드별로 트랜젝션을 설정하고 싶은 경우
  @Transactional(propagation = Propagation.REQUIRED)
  @Override
  public void sendMoney() throws Exception {
    accountDAO.updateBalance1();
    accountDAO.updateBalance2();
  }
}
