package com.spring.mybatis.service;

import java.util.List;

import com.spring.mybatis.entity.MemberVO;

public interface MemberService {
	public List<MemberVO> listMembers(String name, String email);

	public int addMember(MemberVO memberVO);
	
	public int addMembers(List<MemberVO> members);

	public MemberVO getMember(String id);

	public int modMember(MemberVO vo);

	public int removeMember(String id);
	
	public int removeMembers(String[] ids);
	
	public int countMember();
}
