package com.spring.mybatis.service;

public interface AccountService {
  public void sendMoney() throws Exception;
}
