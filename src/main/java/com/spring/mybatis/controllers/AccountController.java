package com.spring.mybatis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.spring.mybatis.service.AccountService;

//컨트롤 클릭하면 링크 열어짐
//http://localhost:8080/ex1/account/remitt.my

@Controller
public class AccountController {
	@Autowired
	private AccountService accountService;

	public AccountController() {
	}

	public void setMemberService(AccountService accountService) {
		this.accountService = accountService;
	}

	@RequestMapping("/account/remitt.my")
  public ModelAndView sendMoney() throws Exception {
     ModelAndView mav=new ModelAndView();
     accountService.sendMoney();
     mav.setViewName("account/result");
     return mav;
  }
}
