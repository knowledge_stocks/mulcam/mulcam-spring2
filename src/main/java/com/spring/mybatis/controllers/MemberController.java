package com.spring.mybatis.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.spring.mybatis.entity.MemberVO;
import com.spring.mybatis.service.MemberService;

//컨트롤 클릭하면 링크 열어짐
//http://localhost:8080/ex1/member/list.my

@Controller
public class MemberController {
	@Autowired
	private MemberService memberService;

	public MemberController() {
	}

	public void setMemberService(MemberService memberService) {
		this.memberService = memberService;
	}

	@RequestMapping(value = "/member/list.my", method = RequestMethod.GET)
	public ModelAndView list(ModelAndView mv,
	    @RequestParam(required = false) String field, @RequestParam(required = false) String query,
			@RequestParam(required = false) String name, @RequestParam(required = false) String email) {
	  if(field != null) {
	    if(field.equals("name")) {
	      name = query;
	    } else if(field.equals("email" )) {
	      email = query;
	    }
	  }
	  
		List<MemberVO> memberList = memberService.listMembers(name, email);
		mv.addObject("members", memberList);
		mv.setViewName("member/list-my");

		return mv;
	}

	@RequestMapping(value = "/member/add.my", method = RequestMethod.GET)
	public String add() {
		return "member/add-my";
	}

	@RequestMapping(value = "/member/add.my", method = RequestMethod.POST)
	public String add(RedirectAttributes redirect, @ModelAttribute MemberVO member) {
		if (memberService.addMember(member) == 1) {
			redirect.addFlashAttribute("msg", "added");
		} else {
			redirect.addFlashAttribute("msg", "failed");
		}
		return "redirect:/member/list.my";
	}
	
	@RequestMapping(value = "/member/addMembers.my", method = RequestMethod.GET)
  public String addMembers(RedirectAttributes redirect) {
	  List<MemberVO> members = new ArrayList<MemberVO>();
	  members.add(new MemberVO("id0000", "1234", "name1234", "email@sample.com", null));
	  members.add(new MemberVO("id0001", "1234", "name1234", "email@sample.com", null));
	  members.add(new MemberVO("id0002", "1234", "name1234", "email@sample.com", null));
	  
	  if (memberService.addMembers(members) > 0) {
      redirect.addFlashAttribute("msg", "added");
    } else {
      redirect.addFlashAttribute("msg", "failed");
    }
    return "redirect:/member/list.my";
  }
	
	@RequestMapping(value = "/member/modify.my", method = RequestMethod.GET)
	public ModelAndView modify(ModelAndView mv, @RequestParam(value="id") String id) {
		MemberVO member = memberService.getMember(id);
		mv.addObject("member", member);
		mv.setViewName("member/modify-my");
		
		return mv;
	}
	
	@RequestMapping(value = "/member/modify.my", method = RequestMethod.POST)
	public String modify(RedirectAttributes redirect, @ModelAttribute MemberVO member) {
		if (memberService.modMember(member) == 1) {
			redirect.addFlashAttribute("msg", "modified");
		} else {
			redirect.addFlashAttribute("msg", "failed");
		}
		return "redirect:/member/list.my";
	}
	
	@RequestMapping(value = "/member/remove.my", method = RequestMethod.GET)
	public String remove(RedirectAttributes redirect, @RequestParam(value="id") String id) {
		if (memberService.removeMember(id) == 1) {
			redirect.addFlashAttribute("msg", "deleted");
		} else {
			redirect.addFlashAttribute("msg", "failed");
		}
		return "redirect:/member/list.my";
	}
	
	@ResponseBody
	@RequestMapping("/member/removeSelected.my")
	public String removeSelected(@RequestParam(value="ids[]") String[] ids) {
		memberService.removeMembers(ids);
		return "succeed";
	}
}
