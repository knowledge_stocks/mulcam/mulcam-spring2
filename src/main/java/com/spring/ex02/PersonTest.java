package com.spring.ex02;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.spring.ex01.PersonService;

public class PersonTest {

	// test 코드
	public static void main(String[] args) {
		// bean으로 설정하지 않고 호출할 때는 이렇게 직접 참조했었다.
//		PersonService service = new PersonServiceImp();
		
		String path = "com/spring/ex02/person.xml";
		
		ApplicationContext context = new ClassPathXmlApplicationContext(path);
		PersonService service1 = (PersonService) context.getBean("service");
		service1.sayHello();
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		// 5초의 텀을 주었지만 hire의 ref가 동일하기 때문에
		// servie1과 service2의 hire은 동일한 인스턴스를 참조한다. 
		PersonService service2 = (PersonService) context.getBean("service2");
		service2.sayHello();
	}
}
