package com.spring.ex01;

import java.util.Date;

public class PersonServiceImp implements PersonService {
	private String name;
	private int age;
	private Date hire;
	
	public PersonServiceImp() {
		
	}
	
	public PersonServiceImp(String name, int age, Date hire) {
		this.name = name;
		this.age = age;
		this.hire = hire;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setAge(int age) {
		this.age = age;
	}
	
	public void setHire(Date hire) {
		this.hire = hire;
	}
	
	@Override
	public void sayHello() {
		System.out.println("이름: " + name);
		System.out.println("나이: " + age);
		System.out.println("고용날짜: " + hire);
	}
}
