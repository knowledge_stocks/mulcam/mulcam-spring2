package com.spring.ex01;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class PersonTest {

	// test 코드
	public static void main(String[] args) {
		// bean으로 설정하지 않고 호출할 때는 이렇게 직접 참조했었다.
//		PersonService service = new PersonServiceImp();
		
		String path = "com/spring/ex01/person.xml";
		
		ApplicationContext context = new ClassPathXmlApplicationContext(path);
		PersonService service = (PersonService) context.getBean("service");
		service.sayHello();
	}
}
