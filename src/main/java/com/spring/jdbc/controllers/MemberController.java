package com.spring.jdbc.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.spring.jdbc.entity.MemberVO;
import com.spring.jdbc.service.MemberService;

// 컨트롤 클릭하면 링크 열어짐
// http://localhost:8080/ex1/member/list.do

@Controller
public class MemberController {
	@Autowired
	private MemberService memberService;

	public MemberController() {
	}

	public void setMemberService(MemberService memberService) {
		this.memberService = memberService;
	}

	@RequestMapping(value = "/member/list.do", method = RequestMethod.GET)
	public ModelAndView list(ModelAndView mv) {
		List<MemberVO> memberList = memberService.listMembers();
		mv.addObject("members", memberList);
		mv.setViewName("member/list");

		return mv;
	}

	@RequestMapping(value = "/member/add.do", method = RequestMethod.GET)
	public String add() {
		return "member/add";
	}

	@RequestMapping(value = "/member/add.do", method = RequestMethod.POST)
	public String add(RedirectAttributes redirect, @ModelAttribute MemberVO member) {
		if (memberService.addMember(member) == 1) {
			redirect.addFlashAttribute("msg", "added");
		} else {
			redirect.addFlashAttribute("msg", "failed");
		}
		return "redirect:/member/list.do";
	}
	
	@RequestMapping(value = "/member/modify.do", method = RequestMethod.GET)
	public ModelAndView modify(ModelAndView mv, @RequestParam(value="id") String id) {
		MemberVO member = memberService.getMember(id);
		mv.addObject("member", member);
		mv.setViewName("member/modify");
		
		return mv;
	}
	
	@RequestMapping(value = "/member/modify.do", method = RequestMethod.POST)
	public String modify(RedirectAttributes redirect, @ModelAttribute MemberVO member) {
		if (memberService.modMember(member) == 1) {
			redirect.addFlashAttribute("msg", "modified");
		} else {
			redirect.addFlashAttribute("msg", "failed");
		}
		return "redirect:/member/list.do";
	}
	
	@RequestMapping(value = "/member/remove.do", method = RequestMethod.GET)
	public String remove(RedirectAttributes redirect, @RequestParam(value="id") String id) {
		if (memberService.removeMember(id) == 1) {
			redirect.addFlashAttribute("msg", "deleted");
		} else {
			redirect.addFlashAttribute("msg", "failed");
		}
		return "redirect:/member/list.do";
	}
}
