package com.spring.jdbc.service;

import java.util.List;

import com.spring.jdbc.entity.MemberVO;

public interface MemberService {
	public List<MemberVO> listMembers();

	public int addMember(MemberVO memberVO);

	public MemberVO getMember(String id);

	public int modMember(MemberVO vo);

	public int removeMember(String id);
	
	public int countMember();
}
