package com.spring.jdbc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.jdbc.entity.MemberVO;
import com.spring.jdbc.repository.MemberDAO;

@Service
public class MemberServiceImp implements MemberService {
	@Autowired
	private MemberDAO memberDAO;

	public MemberServiceImp() {
	}

	public void setMemberDAO(MemberDAO memberDAO) {
		this.memberDAO = memberDAO;
	}

	@Override
	public List<MemberVO> listMembers() {
		return memberDAO.list();
	}

	@Override
	public int addMember(MemberVO memberVO) {
		return memberDAO.insert(memberVO);
	}

	@Override
	public MemberVO getMember(String id) {
		return memberDAO.get(id);
	}

	@Override
	public int modMember(MemberVO vo) {
		return memberDAO.update(vo);
	}

	@Override
	public int removeMember(String id) {
		return memberDAO.delete(id);
	}
	
	@Override
	public int countMember() {
		return memberDAO.count();
	}
}
