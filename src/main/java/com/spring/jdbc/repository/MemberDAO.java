package com.spring.jdbc.repository;

import java.util.List;

import com.spring.jdbc.entity.MemberVO;

public interface MemberDAO {
	public List<MemberVO> list();

	public int insert(MemberVO vo);

	public int update(MemberVO vo);

	public int delete(String id);

	public MemberVO get(String id);

	public int count();
}
