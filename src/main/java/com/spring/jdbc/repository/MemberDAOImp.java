package com.spring.jdbc.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.spring.jdbc.entity.MemberVO;

/*
 * Template 클래스
 * 1. 개발자가 중복된 코드를 입력해야 하는 작업을 줄일 수 있도록 돕고 있다.
 * 2. JDBC뿐만 아니라 myBatis, JMS와 같은 다양한 기술에 대해 템플릿을 제공한다.
 * 3. 예로 Jdbc인 경우 JdbcTemplate클래스를 제공하고 있으며, JdbcTemplate클래스를
 *    사용함으로써 try~cath~finally 및 커넥션 관리를 위한 중복된 코드를 줄이거나
 *    없앨 수 있다.
 *    
 *                             Java JDBC              Spring JDBC
 *    select  				   executeQuery( )         query( )
 *    insert, update, delete   executeUpdate( )        update( )
 */
@Repository
public class MemberDAOImp implements MemberDAO {
	@Autowired
	private JdbcTemplate jdbcTemplate;

	public MemberDAOImp() {
	}
	
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	@Override
	public List<MemberVO> list() {
		String sql = "SELECT"
				+ " id, pwd, name, email, joinDate"
				+ " FROM t_member"
				+ " ORDER BY joinDate DESC";
		List<MemberVO> list = jdbcTemplate.query(sql, new RowMapper<MemberVO>() {
			@Override
			public MemberVO mapRow(ResultSet rs, int rowNum) throws SQLException {
				String id = rs.getString("id");
				String pwd = rs.getString("pwd");
				String name = rs.getString("name");
				String email = rs.getString("email");
				Date joinDate = rs.getDate("joinDate");
				
				return new MemberVO(id, pwd, name, email, joinDate);
			}
		});
		return list;
	}

	@Override
	public int insert(MemberVO vo) {
		String sql = "INSERT INTO t_member"
				+ " (id, pwd, name, email)"
				+ " VALUES"
				+ " (?, ?, ?, ?)";
		int result = jdbcTemplate.update(sql,
				vo.getId(), vo.getPwd(), vo.getName(), vo.getEmail());
		return result;
	}

	@Override
	public int update(MemberVO vo) {
		String sql = "UPDATE t_member"
				+ " SET"
				+ " pwd = ?"
				+ ",name = ?"
				+ ",email = ?"
				+ " WHERE"
				+ " id = ?";
		int result = jdbcTemplate.update(sql,
				vo.getPwd(), vo.getName(), vo.getEmail(), vo.getId());
		return result;
	}

	@Override
	public int delete(String id) {
		String sql = "DELETE FROM t_member"
				+ " WHERE"
				+ " id = ?";
		int result = jdbcTemplate.update(sql, id);
		return result;
	}

	@Override
	public MemberVO get(String id) {
		String sql = "SELECT"
				+ " id, pwd, name, email, joinDate"
				+ " FROM t_member"
				+ " WHERE"
				+ " id = ?";
		MemberVO member = jdbcTemplate.queryForObject(sql, new RowMapper<MemberVO>() {
			@Override
			public MemberVO mapRow(ResultSet rs, int rowNum) throws SQLException {
				String id = rs.getString("id");
				String pwd = rs.getString("pwd");
				String name = rs.getString("name");
				String email = rs.getString("email");
				Date joinDate = rs.getDate("joinDate");
				
				return new MemberVO(id, pwd, name, email, joinDate);
			}
		}, id);
		return member;
	}

	@Override
	public int count() {
		return jdbcTemplate.queryForObject("SELECT count(id) FROM t_member", int.class);
	}
}
