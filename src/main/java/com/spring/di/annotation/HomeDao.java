package com.spring.di.annotation;

import java.util.List;

public interface HomeDao {
	public List<HomeVo> getHomes();
}
