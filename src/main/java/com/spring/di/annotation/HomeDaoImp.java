package com.spring.di.annotation;

import java.util.ArrayList;
import java.util.List;

//@Repository("dao") // <bean id="dao" class="..."/>랑 같은 의미
public class HomeDaoImp implements HomeDao {
	@Override
	public List<HomeVo> getHomes() {
		return new ArrayList<HomeVo>();
	}
}
