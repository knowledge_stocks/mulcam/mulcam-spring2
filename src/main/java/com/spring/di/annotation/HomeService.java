package com.spring.di.annotation;

public interface HomeService {
	public void setDao(HomeDao dao);
	public void process();
}
