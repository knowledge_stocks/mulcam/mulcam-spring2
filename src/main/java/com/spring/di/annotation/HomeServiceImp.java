package com.spring.di.annotation;

import org.springframework.beans.factory.annotation.Autowired;

// <bean id="service" class="...">
//   <property name="dao" ref="dao" />
// </bean>
// 같은 의미?
//@Service("service")
public class HomeServiceImp implements HomeService {
	@Autowired
	private HomeDao dao;

	public HomeServiceImp() {

	}

	@Override
	public void setDao(HomeDao dao) {
		this.dao = dao;
	}

	@Override
	public void process() {
		System.out.println("HomeServiceImp process()" + dao);
	}
}
