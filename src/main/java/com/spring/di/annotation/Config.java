package com.spring.di.annotation;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {
	public Config() {

	}

	@Bean
	public HomeService service() {
		return new HomeServiceImp();
	}
	
	@Bean(name="svc")
	public HomeService service2() {
		return new HomeServiceImp();
	}
	
	@Bean
	public HomeDao dao() {
		return new HomeDaoImp();
	}
}
