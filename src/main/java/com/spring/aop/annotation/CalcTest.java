package com.spring.aop.annotation;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class CalcTest {
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("com/spring/aop/annotation/AOPTest.xml");
		Calculator cal2 = (Calculator) context.getBean("calcTarget");

		cal2.prn1();
		System.out.println();
		cal2.prn2();
		System.out.println();
		cal2.addAndReturn(5, 20);
	}
}
