package com.spring.aop.annotation;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

@Aspect
public class LoggingAdvice2 {
	@Around(value = "execution(* com.spring.aop.annotation.Calculator.addAndReturn(*,*))")
	public Object loggerAop(ProceedingJoinPoint joinpoint) throws Throwable {
		System.out.println("[메서드 호출 전: LoggingAdvice]");
		System.out.println(joinpoint.getSignature().toShortString() + "메서드 호출 전");

		Object object = joinpoint.proceed();
		System.out.println("결과값: " + object);

		System.out.println("[메서드 호출 후: LoggingAdvice]");
		System.out.println(joinpoint.getSignature().toShortString() + "메서드 호출 후");
		return object;
	}

	@Before(value = "execution(* com.spring.aop.annotation.Calculator.prn1(..))")
	public void comm1() {
		System.out.println("comm1 호출됨");
	}

	@AfterReturning(value = "execution(* com.spring.aop.annotation.Calculator.prn2())",
			returning = "val")
	public void comm2(String val) {
		System.out.println("comm2 호출됨. 리턴값: " + val);
	}
}
