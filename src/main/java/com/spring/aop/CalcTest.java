package com.spring.aop;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class CalcTest {
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("com/spring/aop/AOPTest.xml");
//		Calculator cal = (Calculator) context.getBean("proxyCal");
//
//		cal.add(100, 20);
//		System.out.println();
//		cal.subtract(100, 20);
//		System.out.println();
//		cal.multiply(100, 20);
//		System.out.println();
//		cal.divide(100, 20);
//		System.out.println();

		// aop:config로 정의해서 그런지 proxyCal로는 bean을 가져올 수 없었다. 
		Calculator cal2 = (Calculator) context.getBean("calcTarget");

		cal2.prn1();
		System.out.println();
		cal2.prn2();
		System.out.println();
		cal2.addAndReturn(5, 20);
	}
}
