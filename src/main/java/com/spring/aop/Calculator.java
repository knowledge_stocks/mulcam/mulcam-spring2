package com.spring.aop;

public class Calculator {
	public void add(int x, int y) {
		int result = x + y;
		System.out.println("결과: " + result);
	}

	public void subtract(int x, int y) {
		int result = x - y;
		System.out.println("결과: " + result);
	}

	public void multiply(int x, int y) {
		int result = x * y;
		System.out.println("결과: " + result);
	}

	public void divide(int x, int y) {
		int result = x / y;
		System.out.println("결과: " + result);
	}

	public void prn1() {
		System.out.println("prn1 호출됨");
	}

	public String prn2() {
		System.out.println("prn2 호출됨");
		return "asdf";
	}
	
	public int addAndReturn(int a, int b) {
		System.out.println("addAndReturn 호출됨");
		return a + b;
	}
}
