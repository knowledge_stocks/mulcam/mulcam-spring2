package com.spring.aop;

import org.aspectj.lang.ProceedingJoinPoint;

public class LoggingAdvice2 {
	public Object loggerAop(ProceedingJoinPoint joinpoint) throws Throwable {
		System.out.println("[메서드 호출 전: LoggingAdvice]");
		System.out.println(joinpoint.getSignature().toShortString() + "메서드 호출 전");

		Object object = joinpoint.proceed();
		System.out.println("결과값: " + object);

		System.out.println("[메서드 호출 후: LoggingAdvice]");
		System.out.println(joinpoint.getSignature().toShortString() + "메서드 호출 후");
		return object;
	}

	public void comm1() {
		System.out.println("comm1 호출됨");
	}

	public void comm2(String val) {
		System.out.println("comm2 호출됨. 리턴값: " + val);
	}
}
